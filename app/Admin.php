<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Admin extends Model
{
    //
    protected $guard = 'admin';
    protected $fillable = ['name', 'email', 'username', 'password', 'email_verified_at'];
    // protected $hidden = ['password'];
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }
}
