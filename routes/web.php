<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('admin/login', 'Auth\AdminAuthController@getLogin')->name('admin.login');
// Route::post('admin/login', 'Auth\AdminAuthController@postLogin');
Route::middleware('auth:admin')->group(function () {
    // Route::get('admin/logout', 'Auth\AdminAuthController@getLogout')->name('admin.logout');
    // Route::get('admin/home', 'AdminController@index')->name('admin.home');
    // Route::get('admin/login', 'Auth\AdminAuthController@getLogin')->name('admin.login');
    // Route::post('admin/login', 'Auth\AdminAuthController@postLogin');
});

Route::get('admin/home', 'HomeController@adminHome')->name('admin.adminHome')->middleware('is_admin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
